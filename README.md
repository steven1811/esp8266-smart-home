## Synopsis

You can control your Home with this. It uses the ESP8266 WiFi Module to connect to the internet and Data can be exchanged via the internet to control your home.

## Motivation

This is a project for our study in Computer Engineering (PCB design).

## Contributors

Steven Bradley, Lars Pfeiler

## License

Copyright (C) <2016>  <Steven Bradley, Lars Pfeiler>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.