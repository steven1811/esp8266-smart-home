#include <Wire.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>

#include "LCDDriver\LCD.h"
#include "SiteCreator\SiteCreator.h"
#include "LM75Driver\LM75.h"
#include "PCF8574Driver\PCF8574.h"
#include "LCDExtended.h"

//I2C Addresses
#define DISPLAY_ADDRESS 0x27
#define OUTPUT_DRIVER_ADDRESS  0x38
#define TEMP_SENSOR_ADDRESS 0x48

//Configuration
#define APName "SmartHome"
#define PASSWORD "12345678"
#define URL "smarthome.bug"
#define MESSAGE_DELAY 5000

//Pin definitions
#define SDA_PIN 4
#define SCL_PIN 5

#define ONBOARD_LED 2

#define UP_BTN 16
#define RIGHT_BTN 14
#define DOWN_BTN 13
#define LEFT_BTN 15
#define CENTER_BTN 12

const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;
ESP8266WebServer webServer(80);
LCD lcd(DISPLAY_ADDRESS, Wire, LCD_16X2);
LCDExtended lcdE(&lcd,2);
LM75 tempSensor(TEMP_SENSOR_ADDRESS, Wire);
PCF8574 output(Wire, 0x0, PCF8574A_);
String connectedTo="";

#include "EEPROMHandler.h"
#include "WiFiHandler.h"
#include "Webserver.h"

void showConnectInfo() {
	lcd.clear();
	lcd.printAt("AP: " + String(APName), 0);
	lcd.printAt("PW: " + String(PASSWORD), 1);
}

void showURL() {
	lcd.clear();
	lcd.printAt("Connected visit:", 0);
	lcd.printAt(String(URL), 1);
}

boolean checkFactoryReset() {
	ulong current;
	ulong intervall = 3000;
	ulong buttonIntervall = 250;

	lcd.printAt("Booting...", 0);
	lcd.printAt("RESET NOW!", 1);

	while (1) {
		//Wait 3 Seconds before booting up	
		current = millis();

		if (current  >= intervall) {
			return false;
		}

		if (current >= buttonIntervall) {
			if ((digitalRead(DOWN_BTN) == HIGH) && (digitalRead(RIGHT_BTN) == HIGH)) {
				lcd.clear();
				lcd.printAt("Executing", 0);
				lcd.printAt("Factory Reset!", 1);
				delay(3000);
				return true;
			}
		}
	}
}

void setupPins() {
	pinMode(ONBOARD_LED, OUTPUT);
	digitalWrite(ONBOARD_LED, LOW);

	pinMode(UP_BTN, INPUT);
	pinMode(RIGHT_BTN, INPUT);
	pinMode(DOWN_BTN, INPUT);
	pinMode(LEFT_BTN, INPUT);
	pinMode(CENTER_BTN, INPUT);
}

void setupI2C() {
	Wire.begin(SDA_PIN, SCL_PIN);
}

void setupLCD() {
	lcd.begin();
	lcd.setBackgroundLight(true);
}

void setupWiFi() {
	WiFi.mode(WIFI_AP);
	WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
	WiFi.softAP(APName, PASSWORD);
	WiFi.hostname(APName);
}

void setupDNS() {
	dnsServer.setTTL(300);
	dnsServer.setErrorReplyCode(DNSReplyCode::ServerFailure);
	dnsServer.start(DNS_PORT, URL, apIP);
}

void setup() {
	setupPins();
	setupI2C();
	output.writeData(0xFF);
	setupEEPROM();
	setupLCD();
	setupWiFi();

	if (checkFactoryReset()) {
		//Write default parameters to EEPROM
	}
	else {
		//Load parameters from EEPROM
	}

	showConnectInfo();
	setupDNS();
	setupWebserver();
}
uint8 counter = 0;
void handleInput() {
	if (digitalRead(UP_BTN) == HIGH) {

	}

	if (digitalRead(RIGHT_BTN) == HIGH) {
		output.writeData(0xFF);
	}

	if (digitalRead(DOWN_BTN) == HIGH) {
		output.writeData(rand() % 256);
		/*output.writeData(counter);
		if (counter == 0xFF) {
			counter = 0;
		}
		counter++;*/
		delay(35);
	}

	if (digitalRead(LEFT_BTN) == HIGH) {
		output.writeData(0x00);
	}

	if (digitalRead(CENTER_BTN) == HIGH) {
		String temperature = String(tempSensor.TempRead());
		String messages[] = { "Temperature:", temperature + "C" };
		lcdE.delayMessage(messages, 3000);
	}
}

void loop() {
	dnsServer.processNextRequest();
	webServer.handleClient();
	handleInput();
	lcdE.update();
}