#pragma once

//Page creators
void createStatusPage(SiteCreator &creator) {
	creator.setSection("Status");
	creator.createForm("Relay Control", "index.htm", "POST");
	creator.addFormSubmit("Channel0", "Toggle", "Channel 0:");
	creator.addFormSubmit("Channel1", "Toggle", "Channel 1:");
	creator.addFormSubmit("Channel2", "Toggle", "Channel 2:");
	creator.addFormSubmit("Channel3", "Toggle", "Channel 3:");
	creator.addFormSubmit("Channel4", "Toggle", "Channel 4:");
	creator.addFormSubmit("Channel5", "Toggle", "Channel 5:");
	creator.addFormSubmit("Channel6", "Toggle", "Channel 6:");
	creator.addFormSubmit("Channel7", "Toggle", "Channel 7:");

	creator.finishForm();

	if (!connectedTo.equals("")) {
		creator.println("Connected to Station: " + connectedTo);
	}
	else {
		creator.println("Not connected to Station!");
	}

	creator.println("Room temperature: " + String(tempSensor.TempRead()) + "C");
	creator.println("<br>Relay status:");
	for (uint8 i = 0; i <= 7; i++) {
		String status = "";
		if (!output.readBit(i)) {
			status = "ON!";
		}
		else {
			status = "OFF!";
		}
		creator.println("Channel " + String(i) + ": " + status);
	}
}

void createConfigPage(SiteCreator &creator) {
	creator.setSection("Configuration");
	creator.createForm("WIFI Configuration", "config.htm", "POST");
	creator.addFormText("SSIDInput", "", "SSID:");
	creator.addFormPassword("PasswordInput", "", "Password:");
	creator.addFormCheckbox("Autoconnect", "", "Autoconnect:", false);
	creator.addFormSubmit("ConnectBtn", "Connect", "");
	creator.finishForm();
}

void createAboutPage(SiteCreator &creator) {
	creator.setSection("About");
	creator.println("Made by:");
	creator.println("LCD Hitachi HD44780 I2C Driver: Steven Bradley");
	creator.println("LM75 Temperature Sensor I2C Driver: Lars Pfeiler");
	creator.println("Board Layout and Design: Lars Pfeiler, Steven Bradley");
	creator.println("Webserver: Steven Bradley");
}




//Handling of the Webserver
void setupWebserver() {
	webServer.onNotFound([]() {
		String message = "Page not found!\n\n";
		message += "URI: ";
		message += webServer.uri();
		webServer.send(404, "text/plain", message);
	});

	webServer.on("/", HTTPMethod::HTTP_GET, []() {
		SiteCreator creator(webServer);
		createStatusPage(creator);
		creator.send();
	});

	webServer.on("/index.htm", HTTPMethod::HTTP_GET, []() {
		SiteCreator creator(webServer);
		createStatusPage(creator);
		creator.send();
	});

	webServer.on("/status.htm", HTTPMethod::HTTP_GET, []() {
		SiteCreator creator(webServer);
		createStatusPage(creator);
		creator.send();
	});

	webServer.on("/about.htm", HTTPMethod::HTTP_GET, []() {
		SiteCreator creator(webServer);
		createAboutPage(creator);
		creator.send();
	});

	webServer.on("/config.htm", HTTPMethod::HTTP_GET, []() {
		SiteCreator creator(webServer);
		createConfigPage(creator);
		creator.send();
	});

	webServer.on("/index.htm", HTTPMethod::HTTP_POST, []() {
		SiteCreator creator(webServer);

		int argCount = webServer.args();

		//Go through Post Data
		for (int i = 0; i < argCount; i++) {
			String argName = webServer.argName(i);
			String argValue = webServer.arg(i);

			for (uint8 i = 0; i <= 7; i++) {
				if (argName.equals("Channel" + String(i))) {
					if (argValue.equals("Toggle")) {
						output.writeBit(i, !output.readBit(i));
						String messageToggle[] = { "Channel " + String(i) + ":", "" };
						if (output.readBit(i)) {
							messageToggle[1] = "OFF!";
						}
						else {
							messageToggle[1] = "ON!";
						}
						lcdE.delayMessage(messageToggle, MESSAGE_DELAY);
					}
				}
			}
		}

		createStatusPage(creator);
		creator.send();
	});

	webServer.on("/config.htm", HTTPMethod::HTTP_POST, []() {
		String ssid;
		String pw;

		SiteCreator creator(webServer);

		int argCount = webServer.args();

		//Go through Post Data
		for (int i = 0; i < argCount; i++) {
			String argName = webServer.argName(i);
			String argValue = webServer.arg(i);

			if (argName.equals("SSIDInput")) {
				ssid = argValue;
			}

			if (argName.equals("PasswordInput")) {
				pw = argValue;
			}
		}

		connectToAP(ssid.c_str(), pw.c_str());

		createConfigPage(creator);
		creator.send();
	});

	webServer.begin();
}