#ifndef LM75_H
#define LM75_H

#define ControlRegister 0x00
#define SizesBytes 0x02

class LM75 
{
public:
	TwoWire &mWire;

	LM75(uint8 Address, TwoWire &wire) : mAddress(Address), mWire(wire) {};
	float TempRead()
	{
		uint16 temp = 0;
		uint8 low;
		uint8 high;

		mWire.beginTransmission(mAddress);
		mWire.write(ControlRegister);
		mWire.endTransmission();
		mWire.requestFrom(uint8(mAddress), uint8(SizesBytes)); //forder 2 Bytes an, die gelesen werden

		while (Wire.available()) {
			high = Wire.read();
			low = Wire.read();
		}
		temp = (high << 8) | low;
		float tempCalc = (temp >> 5) * 0.125F;
		return tempCalc;
	};

private:
	uint8 mAddress;
};

#endif 