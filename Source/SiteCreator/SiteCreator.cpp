#include "SiteCreator.h"

void SiteCreator::setSection(String txt) {
	site.replace(SECTION_TAG, txt);
}

void SiteCreator::print(String txt) {
	site.replace(CONTENT_TAG, txt + String(CONTENT_TAG));
}

void SiteCreator::println(String txt) {
	site.replace(CONTENT_TAG, txt + "<br>" + String(CONTENT_TAG));
}

void SiteCreator::createForm(String title, String actionPage, String method) {
	site.replace(CONTENT_TAG, "<form action=\"" + actionPage + "\" method=\"" + method + "\"><fieldset><legend>" + title + "</legend>" + String(FORM_TAG) + "</fieldset>" + "</form><br>" + String(CONTENT_TAG));
}

void SiteCreator::addFormButton(String name, String value, String label) {
	site.replace(FORM_TAG, label + "<input type=\"button\" name=\"" + name + "\" value=\"" + value + "\" >" + "<br>" + FORM_TAG);
}


void SiteCreator::addFormSubmit(String name, String value, String label) {
	site.replace(FORM_TAG, label + "<input type=\"submit\" name=\"" + name + "\" value=\"" + value + "\" >" + "<br>" + FORM_TAG);
}

void SiteCreator::addFormCheckbox(String name, String value, String label, boolean checked) {
	//<input type="checkbox" name="vehicle" value="Car" checked>
	String check = "";
	
	if (checked) {
		check = "checked";
	}

	site.replace(FORM_TAG, label + "<input type=\"checkbox\" name=\"" + name + "\" value=\"" + value + "\"" + check + " >" + "<br>" + FORM_TAG);
}

void SiteCreator::addFormText(String name, String value, String label) {
	site.replace(FORM_TAG, label + "<input type=\"text\" name=\"" + name + "\" value=\"" + value + "\" />" + "<br>" + FORM_TAG);
}

void SiteCreator::addFormPassword(String name, String value, String label) {
	site.replace(FORM_TAG, label + "<input type=\"password\" name=\"" + name + "\" value=\"" + value + "\" />" + "<br>" + FORM_TAG);
}



void SiteCreator::finishForm() {
	site.replace(FORM_TAG, "");
}

void SiteCreator::send() {
	site.replace(CONTENT_TAG, "");
	mServer.send(200, "text/html", site);
}