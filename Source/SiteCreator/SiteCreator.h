#ifndef SITECREATOR_H
#define SITECREATOR_H

#include <Arduino.h>
#include <ESP8266WebServer.h>

#define CONTENT_TAG "?CONTENT?"
#define SECTION_TAG "?SECTION?"
#define FORM_TAG "?FORM?"

class SiteCreator {
public:
	String site =
		"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /><title>ESP8266 SmartHome</title><style>* {margin: 0;padding: 0;}html {height: 100%;}body {font: 18px Arial, sans-serif;color: #FFFFFF;width: 100%;height: 100%;background-color: #1a2740;}.wrapper {width: 1000px;margin: 0 auto;min-height: 100%;height: auto !important;height: 100%;}.header {font: 28px Arial, sans-serif;height: 150px;background: #FFE680;text-align: center;background-color: #6391b0;}.middle {width: 100%;padding: 0 0 100px;position: relative;}.middle:after {display: table;clear: both;content: '';}.container {width: 100%;float: left;overflow: hidden;}.content {padding: 0 0 0 270px;position: relative;height: 738px;background-color: #346d8c;}.navi {float: left;height: 738px;margin-left: -100%;position: relative;background: #245878;}.navi ul { list-style-type: none; margin: 0; padding: 0; width: 200px; background-color: #f1f1f1;}.navi li a { display: block; color: #000; padding: 8px 16px; text-decoration: none;}.navi li a:hover { background-color: #555; color: white;}.footer {font: 16px Arial, sans-serif;text-align: center;width: 1000px;margin: -100px auto 0;height: 100px;background: #6391b0;position: relative;}</style></head><body><div class=\"wrapper\"><div class=\"header\"><p>ESP8266 SmartHome</p>?SECTION?</div><div class=\"middle\"><div class=\"container\"><div class=\"content\">?CONTENT?</div></div><div class=\"navi\"><ul> <li><a href=\"status.htm\">Status</a></li> <li><a href=\"config.htm\">Configuration</a></li> <li><a href=\"about.htm\">About</a></li></ul></div></div></div><div class=\"footer\">ESP8266 SmartHome HTW Berlin</div></body></html>";
	
	SiteCreator(ESP8266WebServer &server) : mServer(server) {};
	void send();
	void setSection(String txt);
	void print(String txt);
	void println(String txt);
	void createForm(String title, String actionPage, String method="GET");
	void addFormButton(String name, String value, String label);
	void addFormSubmit(String name, String value, String label);
	void addFormCheckbox(String name, String value, String label, boolean checked);
	void addFormText(String name, String value, String label);
	void addFormPassword(String name, String value, String label);
	void finishForm();
private:
	ESP8266WebServer &mServer;

};

#endif