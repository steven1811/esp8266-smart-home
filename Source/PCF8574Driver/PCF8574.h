#ifndef PCF8574_H
#define PCF8574_H

#define PCF8574_ADDRESS_MASK 0b01000000
#define PCF8574A_ADDRESS_MASK 0b01110000
#define SHIFT_WITHOUT_RW 1
enum PCF8574_VARIANT { PCF8574_, PCF8574A_ };
class PCF8574 {
public:

	PCF8574(TwoWire &wire, uint8 address, PCF8574_VARIANT variant) : mWire(wire), mVariant(variant) {
		switch (mVariant) {
		case PCF8574_:
			mAddress = (PCF8574_ADDRESS_MASK>> SHIFT_WITHOUT_RW) | address;
			break;
		case PCF8574A_:
			mAddress = (PCF8574A_ADDRESS_MASK>> SHIFT_WITHOUT_RW) | address;
			break;
		}
	};

	uint8 writeData(uint8 data) {
		mWire.beginTransmission(mAddress);
		mWire.write(data);
		return mWire.endTransmission();
	}

	uint8 readData() {
		uint8 data;
		mWire.requestFrom(uint8(mAddress), uint8(1));
		while (mWire.available()) {
			data = mWire.read();
		}
		return data;
	}

	void writeBit(uint8 position, boolean enabled) {
		uint8 data = readData();
		if (enabled) {
			data |= (1 << position);
		}
		else {
			data &= ~(1 << position);
		}
		writeData(data);
	}

	boolean readBit(uint8 position) {
		uint8 tmp = readData();
		tmp &= (1 << position);
		return boolean(tmp);
	}

private:
	TwoWire &mWire;
	PCF8574_VARIANT mVariant;
	uint8 mAddress;
};

#endif