#pragma once

void connectToAP(String ssid, String password) {
	String messageConnecting[] = { "Connecting...",ssid };
	lcdE.delayMessage(messageConnecting, MESSAGE_DELAY);

	if (WiFi.isConnected()) {
		WiFi.disconnect();
	}

	WiFi.begin(ssid.c_str(), password.c_str());
}

static WiFiEventHandler connectedSoftAP = WiFi.onSoftAPModeStationConnected([](const WiFiEventSoftAPModeStationConnected &event) {
	showURL();
});

static WiFiEventHandler disconnectedSoftAP = WiFi.onSoftAPModeStationDisconnected([](const WiFiEventSoftAPModeStationDisconnected &event) {
	showConnectInfo();
});

static WiFiEventHandler disconnectedStation = WiFi.onStationModeDisconnected([](const WiFiEventStationModeDisconnected &event) {
	String messageNoAP[] = { "DISCONNECTED!", "No AP found!" };
	String messageWrongAuth[] = { "DISCONNECTED!", "Wrong Auth.!" };
	connectedTo = "";

	switch (event.reason) {
	case WIFI_DISCONNECT_REASON_NO_AP_FOUND:
		lcdE.delayMessage(messageNoAP, MESSAGE_DELAY);
		break;
	case WIFI_DISCONNECT_REASON_AUTH_FAIL:
		lcdE.delayMessage(messageWrongAuth, MESSAGE_DELAY);
		break;
	}
	WiFi.mode(WiFiMode::WIFI_AP);
	WiFi.hostname(APName);
});

static WiFiEventHandler connectedStation = WiFi.onStationModeConnected([](const WiFiEventStationModeConnected &event) {
	connectedTo = event.ssid;
	String messageConnected[] = { "CONNECTED TO:", connectedTo };
	lcdE.delayMessage(messageConnected, MESSAGE_DELAY);

	WiFi.mode(WiFiMode::WIFI_AP_STA);
	WiFi.hostname(APName);
});