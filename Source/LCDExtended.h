#pragma once
#include <stack>
//Allows non-blocking "Multitasking" for LCD Displays
//Improvements add a stack to display messages

class LCDExtended {
public:
	LCDExtended(LCD *lcd, uint8 lines) : mLcdP(lcd), mLines(lines) {};

	void delayMessage(String messages[], ulong durationMs) {
		if (!mTaskRunning) {
			mTaskRunning = true;
			mDurationMs = durationMs;

			for (uint8 i = 0; i < mLines; i++) {
				mSavedMessages[i] = mLcdP->getText(i);
				mLcdP->printAt(messages[i], i);
			}
			previous = millis();
		}
	}

	void update() {
		current = millis();
		if (((current - previous) > mDurationMs) && mTaskRunning) {
			for (uint8 i = 0; i < mLines; i++) {
				mLcdP->printAt(mSavedMessages[i], i);
			}
			mTaskRunning = false;
		}
	}

private:
	String mSavedMessages[4];
	uint8 mLines;
	ulong mDurationMs;
	LCD *mLcdP;
	boolean mTaskRunning;
	ulong previous;
	ulong current;
};