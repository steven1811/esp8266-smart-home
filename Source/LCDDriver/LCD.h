//ESP8266 16x2 LCD I2C PCF8574T Driver by Steven Bradley

#ifndef LCD_H
#define LCD_H

#include <Wire.h>
#include <Arduino.h>

#include "LCD_Hardware.h"
#include "LCD_Commands.h"

//I2C Map
/*
Backpack:
P7 P6 P5 P4 P3 P2 P1 P0
D7 D6 D5 D4 BT E  RW RS

Mapping:
DB7 DB6 DB5 DB4 BT  E  RW  RS / DB3 DB2 DB1 DB0 BT E RW RS
15  14  13  12  11 10  9   8  / 7   6   5   4   3  2 1  0
*/

//Timing Delays Initialization
#define DELAY_POWER_ON 15000
#define DELAY_POWER_ON2 4100
#define DELAY_POWER_ON3 100

#define PSEUDO_FUNCTION_SET (1 << DB1) | (1 << DB0) //Used for initialization

class LCD : public LCD_Commands {
public:

	LCD(byte I2CAddress, TwoWire &wire, LCD_TYPE type) : LCD_Commands(I2CAddress, wire, type) {};

	//User LCD Functions
	void begin();
	void clear();

	void printChar(char c);
	void printAt(String str, byte line);
	void setCursorPosition(byte x, byte y);
	void setBackgroundLight(boolean enabled);
	void setBlinkingCursor(boolean enabled);
	void setShowCursor(boolean enabled);
	void setShowDisplay(boolean enabled);
	String getText(uint8 lineIndex) {return lineText[lineIndex];};

private:
	char lineText[4][40];

	boolean mCursorShown = false;
	boolean mCursorBlinking = false;
	boolean mDisplayEnabled = true;

	byte mPosX=0; //Keeps track of the cursor
	byte mPosY=0; //Keeps track of the cursor

	//byte readData(); //Not implemented.
	//void readBusyFlagAndAddress(); //Most likely broken! UNTESTED!

};
#endif