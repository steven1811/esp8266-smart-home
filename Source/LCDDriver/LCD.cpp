//ESP8266 16x2 LCD I2C PCF8574T Driver by Steven Bradley

#include "LCD.h"

void LCD::begin() {
	Serial.begin(115200);

	//3 Pseudo Function Sets
	delayMicroseconds(DELAY_POWER_ON);
	instructionWrite(PSEUDO_FUNCTION_SET);
	delayMicroseconds(DELAY_POWER_ON2); //Wait for more than 4.1 ms
	instructionWrite(PSEUDO_FUNCTION_SET);
	delayMicroseconds(DELAY_POWER_ON3); //Wait for more than 100 �s
	instructionWrite(PSEUDO_FUNCTION_SET);

	instructionWrite((1 << DB1)); //4 bit Function Set
	functionSet(); //Complete Function Set
	display(false, false, false); //Display off
	clearDisplay(); //Clear Display
	entryModeSet(true, false); //Set Entry Mode

	functionSet(); //Function set
	display(true, false, false); //Display on
	entryModeSet(true, false); //Entry Mode set
	
	//Make sure that 0x20 is a blank space!
	setCGRAM(0x20); 
	dataWrite(0x20);
}

void LCD::setCursorPosition(byte x, byte y) {
	if ((x < mMaxX) && (y < mMaxY)) {
		mPosX = x;
		mPosY = y;
		setDDRAM(mPosX + mBaseAddresses[mPosY]);
	}
}

void LCD::printChar(char c) {
	lineText[mPosY][mPosX] = c;
	dataWrite(c);

	if (mPosY < mMaxY) {
		if (mPosX < mMaxX-1) {
			mPosX++;
		}
		else {
			mPosX = 0;
			mPosY++;
			setCursorPosition(mPosX, mPosY);
		}
	}
	else {
		mPosY = 0;
		mPosX = 0;
		setCursorPosition(0, 0);
	}
}

void LCD::printAt(String str, byte line) {
	setCursorPosition(0, line);
	for (uint8 i = 0; i < str.length(); i++) {
		printChar(str[i]);
	}

	//Fill the rest with space chars
	for (uint8 i = 0; i < (mMaxX-str.length()); i++) {
		printChar(' ');
	}
}

void LCD::clear() {
	mPosX = 0;
	mPosY = 0;
	for (uint8 y = 0; y < mMaxY; y++) {
		for (uint8 x = 0; x < mMaxX; x++) {
			lineText[y][x] = ' ';
		}
	}
	clearDisplay();
}

void LCD::setBackgroundLight(boolean enabled) {
	mBackgroundLight = enabled;
	i2cTransmit((enabled  << BACKLIGHT_L) | (1 << RW_L));
}

void LCD::setBlinkingCursor(boolean enabled) {
	mCursorShown = enabled;
	display(mDisplayEnabled, mCursorShown, mCursorBlinking);
}

void LCD::setShowCursor(boolean enabled) {
	mCursorBlinking = enabled;
	display(mDisplayEnabled, mCursorShown, mCursorBlinking);
}

void LCD::setShowDisplay(boolean enabled) {
	mDisplayEnabled = enabled;
	display(mDisplayEnabled, mCursorShown, mCursorBlinking);
}