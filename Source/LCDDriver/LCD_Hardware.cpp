#include "LCD_Hardware.h"
LCD_Hardware::LCD_Hardware(byte I2CAddress, TwoWire &wire, LCD_TYPE type) : mI2CAddress(I2CAddress), mWire(wire), mType(type) {
	switch (mType) {
	case LCD_16X1_TYPE1:
		mMaxX = 7;
		mMaxY = 2;
		mBaseAddresses[0] = LCD_16X1_TYPE1_BASE0;
		mBaseAddresses[1] = LCD_16X1_TYPE1_BASE1;
		mFont = CHARACTER_5X8_D8;
		break;
	case LCD_16X1_TYPE2:
		mMaxX = 16;
		mMaxY = 1;
		mBaseAddresses[0] = LCD_16X1_TYPE2_BASE0;
		mFont = CHARACTER_5X8_D8;
		break;
	case LCD_16X2:
		mMaxX = 16;
		mMaxY = 2;
		mBaseAddresses[0] = LCD_16X2_BASE0;
		mBaseAddresses[1] = LCD_16X2_BASE1;
		mFont = CHARACTER_5X8_D16;
		break;
	case LCD_16X4:
		mMaxX = 16;
		mMaxY = 4;
		mBaseAddresses[0] = LCD_16X4_BASE0;
		mBaseAddresses[1] = LCD_16X4_BASE1;
		mBaseAddresses[2] = LCD_16X4_BASE2;
		mBaseAddresses[3] = LCD_16X4_BASE3;
		mFont = CHARACTER_5X8_D16;
		break;
	case LCD_20X2:
		mMaxX = 20;
		mMaxY = 2;
		mBaseAddresses[0] = LCD_20X2_BASE0;
		mBaseAddresses[1] = LCD_20X2_BASE1;
		mFont = CHARACTER_5X8_D16;
		break;
	case LCD_20X4:
		mMaxX = 20;
		mMaxY = 4;
		mBaseAddresses[0] = LCD_20X4_BASE0;
		mBaseAddresses[1] = LCD_20X4_BASE1;
		mBaseAddresses[2] = LCD_20X4_BASE3;
		mBaseAddresses[3] = LCD_20X4_BASE4;
		mFont = CHARACTER_5X8_D16;
		break;
	case LCD_40X2:
		mMaxX = 40;
		mMaxY = 2;
		mBaseAddresses[0] = LCD_40X2_BASE0;
		mBaseAddresses[1] = LCD_40X2_BASE1;
		mFont = CHARACTER_5X8_D16;
		break;
	}
}

void LCD_Hardware::dataWrite(char c) {
	uint8 nibbleHigh = (c & 0xF0);
	uint8 nibbleLow = ((c << 4) & 0xF0);

	i2cTransmit((1 << ENABLE_L) | (1 << RS_L) | nibbleHigh | (mBackgroundLight << BACKLIGHT_L));
	i2cTransmit(0 | (mBackgroundLight << BACKLIGHT_L));

	i2cTransmit((1 << ENABLE_L) | (1 << RS_L) | nibbleLow | (mBackgroundLight << BACKLIGHT_L));
	i2cTransmit(0 | (mBackgroundLight << BACKLIGHT_L));
}

void LCD_Hardware::sendCMD(uint16 command) {
	uint16 newCMD = command | (mBackgroundLight << BACKLIGHT_H) | (1 << ENABLE_H) | (mBackgroundLight << BACKLIGHT_L) | (1 << ENABLE_L);

	uint8 high = (newCMD >> 8);
	uint8 low = newCMD & 0xff;

	instructionWrite(high);
	instructionWrite(low);
}

void LCD_Hardware::instructionWrite(uint8 instruction) {
	i2cTransmit((1 << ENABLE_L) | (mBackgroundLight << BACKLIGHT_L) | instruction);
	i2cTransmit((mBackgroundLight << BACKLIGHT_L) | (1 << RW_L));
}

void LCD_Hardware::i2cTransmit(uint8 data) {
	mWire.beginTransmission(mI2CAddress);
	mWire.write(data);
	mWire.endTransmission();
}