#ifndef LCD_HARDWARE_H
#define LCD_HARDWARE_H

#include <Arduino.h>
#include <Wire.h>
#include "LCD_Config.h"

#define LCD_16X1_TYPE1_BASE0 0x00
#define LCD_16X1_TYPE1_BASE1 0x40

#define LCD_16X1_TYPE2_BASE0 0x00

#define LCD_16X2_BASE0 0x00
#define LCD_16X2_BASE1 0x40

#define LCD_16X4_BASE0 0x00
#define LCD_16X4_BASE1 0x40
#define LCD_16X4_BASE2 0x10
#define LCD_16X4_BASE3 0x50

#define LCD_20X2_BASE0 0x00
#define LCD_20X2_BASE1 0x40

#define LCD_20X4_BASE0 0x00
#define LCD_20X4_BASE1 0x40
#define LCD_20X4_BASE3 0x14
#define LCD_20X4_BASE4 0x54

#define LCD_40X2_BASE0 0x00
#define LCD_40X2_BASE1 0x40

enum LCD_CHARACTER_FONT {
	CHARACTER_5X8_D8, CHARACTER_5X10, CHARACTER_5X8_D16
};

enum LCD_TYPE {
	LCD_16X1_TYPE1,
	LCD_16X1_TYPE2,
	LCD_16X2,
	LCD_16X4,
	LCD_20X2,
	LCD_20X4,
	LCD_40X2
};

//Enumeration

class LCD_Hardware {
protected:
	uint8 mBaseAddresses[4];

	byte mI2CAddress; //Contains the Address of the PCF8574T
	boolean mBackgroundLight = true;

	TwoWire &mWire;

	byte mMaxX = 0; //Max chars per line
	byte mMaxY = 0; //How many lines

	LCD_TYPE mType;
	LCD_CHARACTER_FONT mFont; //Which Charset should be used

	LCD_Hardware(byte I2CAddress, TwoWire &wire, LCD_TYPE type);

	void dataWrite(char c);
	void sendCMD(uint16 command);
	void instructionWrite(uint8 instruction);
	void i2cTransmit(uint8 data);
};
#endif