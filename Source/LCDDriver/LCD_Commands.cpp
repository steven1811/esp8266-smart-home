#include "LCD_Commands.h"

void LCD_Commands::entryModeSet(boolean incrementCursor, boolean shiftDisplay) {
	sendCMD(ENTRY_MODE_SET | (incrementCursor << ENTRY_ID) | (shiftDisplay << ENTRY_S));
	delayMicroseconds(DELAY_ENTRY_MODE_SET);
}

void LCD_Commands::display(boolean displayOn, boolean cursorDisplayed, boolean cursorBlinks) {
	sendCMD((DISPLAY_ON_OFF) | (displayOn << DPY_D) | (cursorDisplayed << DPY_C) | (cursorBlinks << DPY_B));
	delayMicroseconds(DELAY_DISPLAY_ON_OFF);
}

//Most likely broken! UNTESTED!
void LCD_Commands::readBusyFlagAndAddress() {
	uint8 low = 0;
	uint8 high = 0;
	byte receivedByteCount = 0;

	i2cTransmit((1 << ENABLE_L) | (mBackgroundLight << BACKLIGHT_L) | (1 << RW_L));
	receivedByteCount = mWire.requestFrom(uint8_t(mI2CAddress), uint8_t(1));
	while (mWire.available()) {
		high = (mWire.read());
	}
	i2cTransmit((mBackgroundLight << BACKLIGHT_L) | (1 << RW_L));

	i2cTransmit((1 << ENABLE_L) | (mBackgroundLight << BACKLIGHT_L) | (1 << RW_L));
	receivedByteCount = mWire.requestFrom(uint8_t(mI2CAddress), uint8_t(1));
	while (mWire.available()) {
		low = (mWire.read());
	}
	i2cTransmit((mBackgroundLight << BACKLIGHT_L) | (1 << RW_L));

	uint16 complete = ((high<<8) | low);
	if (complete != 0) {
		Serial.println("Read HIGH: 0x" + String(high, 2));
		Serial.println("Read LOW: 0x" + String(low, 2));
		Serial.println("Read Complete: 0x" + String(complete, 2));
	}

}

void LCD_Commands::shift(boolean cursorShift, boolean displayShift) {
	sendCMD(CURSOR_OR_DISPLAY_SHIFT | (cursorShift << SHIFT_SC) | (displayShift << SHIFT_RL));
	delayMicroseconds(DELAY_CURSOR_OR_DISPLAY_SHIFT);
}

void LCD_Commands::setCGRAM(byte address) {
	uint8 highAddress = ((address >> 4) & 03);
	uint8 lowAddress = ((address) & 0x0F);

	uint16 cmd = ((highAddress << 12) | (lowAddress << 4) | SET_CGRAM_ADDRESS);
	sendCMD(cmd);
	delayMicroseconds(DELAY_SET_CGRAM_ADDRESS);
}

void LCD_Commands::returnHome() {
	sendCMD(RETURN_HOME);
	delayMicroseconds(DELAY_RETURN_HOME);
}

void LCD_Commands::functionSet() {
	uint16 cmd = FUNCTION_SET;

	switch (mFont) {
	case LCD_CHARACTER_FONT::CHARACTER_5X8_D8:
		sendCMD(cmd);
		break;
	case LCD_CHARACTER_FONT::CHARACTER_5X10:
		cmd |= (1 << FSET_F);
		sendCMD(cmd);
		break;
	case LCD_CHARACTER_FONT::CHARACTER_5X8_D16:
		cmd |= (1 << FSET_N);
		sendCMD(cmd);
		break;
	}

	delayMicroseconds(DELAY_FUNCTION_SET);
}

void LCD_Commands::clearDisplay() {
	sendCMD(0 | CLEAR_DISPLAY);
	delayMicroseconds(DELAY_CLEAR_DISPLAY);
}

void LCD_Commands::setDDRAM(byte address) {
	uint8 highAddress = ((address >> 4) & 07);
	uint8 lowAddress = ((address) & 0x0F);

	uint16 cmd = ((highAddress << 12) | (lowAddress << 4) | SET_DDRAM_ADDRESS);
	sendCMD(cmd);
	delayMicroseconds(DELAY_SET_DDRAM_ADDRESS);
}

void LCD_Commands::writeData(char data) {
	sendCMD(data | WRITE_DATA);
	delayMicroseconds(DELAY_WRITE_DATA);
}