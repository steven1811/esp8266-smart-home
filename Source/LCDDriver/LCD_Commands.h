#ifndef LCD_COMMANDS_H
#define LCD_COMMANDS_H

#include <Arduino.h>
#include "LCD_Config.h"
#include "LCD_Hardware.h"

//Command Set
#define CLEAR_DISPLAY (1<<DB0)
#define RETURN_HOME (1<<DB1)
#define ENTRY_MODE_SET (1<<DB2)
#define DISPLAY_ON_OFF (1<<DB3)
#define CURSOR_OR_DISPLAY_SHIFT (1<<DB4)
#define FUNCTION_SET (1<<DB5)
#define SET_CGRAM_ADDRESS (1<<DB6)
#define SET_DDRAM_ADDRESS (1<<DB7)
#define READ_BUSY_FLAG_ADDRESS (1<<RW_H) | (1<<RW_L)
#define WRITE_DATA (1<<RS_H) | (1<<RS_L)
#define READ_DATA (1<<RS_H) | (1<<RS_L) | (1<<RW_H) | (1<<RW_L)

//Function Set
#define FSET_DL DB4
#define FSET_N DB3
#define FSET_F DB2

//Display on/off control
#define DPY_D DB2
#define DPY_C DB1
#define DPY_B DB0

//Entry Mode
#define ENTRY_ID DB1
#define ENTRY_S DB0

//Cursor/Display Shift
#define SHIFT_SC DB2
#define SHIFT_RL DB3

//Timing Delays Execution Time
#define DELAY_CLEAR_DISPLAY 1640
#define DELAY_RETURN_HOME 1640
#define DELAY_ENTRY_MODE_SET 40
#define DELAY_DISPLAY_ON_OFF 40
#define DELAY_CURSOR_OR_DISPLAY_SHIFT 40
#define DELAY_FUNCTION_SET 40
#define DELAY_SET_CGRAM_ADDRESS 40
#define DELAY_SET_DDRAM_ADDRESS 40
#define DELAY_READ_BUSY_FLAG_ADDRESS 40
#define DELAY_WRITE_DATA 40
#define DELAY_READ_DATA 40

struct BUSY_ADDRESS {
	boolean busy = false;
	byte address = 0;
};

class LCD_Commands : protected LCD_Hardware {
public:
	void readBusyFlagAndAddress();

protected:
	LCD_Commands(byte I2CAddress, TwoWire &wire, LCD_TYPE type) : LCD_Hardware(I2CAddress, wire, type) {};
	void clearDisplay();
	void setDDRAM(byte address);
	//void readBusyFlagAndAddress();
	void entryModeSet(boolean incrementCursor, boolean shiftDisplay);
	void display(boolean displayOn, boolean cursorDisplayed, boolean cursorBlinks);
	void shift(boolean cursorShift, boolean displayShift);
	void functionSet();
	void setCGRAM(byte address);
	void writeData(char data);
	void returnHome();
	
};

#endif