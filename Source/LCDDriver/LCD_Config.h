//ESP8266 16x2 LCD I2C PCF8574T Driver by Steven Bradley
#ifndef LCD_CONFIG_H
#define LCD_CONFIG_H

//Config
//Upper Bits
#define DB7 15
#define DB6 14
#define DB5 13
#define DB4 12
#define BACKLIGHT_H 11
#define ENABLE_H 10
#define RW_H 9
#define RS_H 8

//Lower Bits
#define DB3 7
#define DB2 6
#define DB1 5
#define DB0 4
#define BACKLIGHT_L 3
#define ENABLE_L 2
#define RW_L 1
#define RS_L 0

#endif