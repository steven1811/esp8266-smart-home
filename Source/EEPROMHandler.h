#pragma once

#define EEPROM_RESERVED 512

typedef struct {
	boolean autoconnect=true;
	String ssid="";
	String password="";
}EEPROMData;

EEPROMData data;

void setupEEPROM() {
	EEPROM.begin(EEPROM_RESERVED);
}

